package task;

import task.TaskImpl;
import java.util.List;
import java.util.ArrayList;
import java.util.StringJoiner;
import java.util.Arrays;

public class TaskFindPrimes extends TaskImpl {
    @Override
    public void execute() {
        String[] stringArray = input.split(",");
        int start = Integer.parseInt(stringArray[0]);
        int end = Integer.parseInt(stringArray[1]);
        result = printList(primes(start, end));
    }

    private  List<Integer> primes(int start, int end) {
        List<Integer> primes = new ArrayList<>();
        for (int i = start; i<= end; i++){
            if (isPrime(i)){
                primes.add(i);
            }
        }
        return primes;
    }

    public boolean isPrime(int n) {
        if (n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    private String printList(List<Integer> list) {
        StringJoiner stringJoiner = new StringJoiner(",", "[","]");
        list.forEach(item -> stringJoiner.add(String.valueOf(item)));
        return stringJoiner.toString();
    }
}